use std::collections::HashMap;

fn main() {
    let buzzer = Buzzer::with_bazz();
    let ints = (1..9)
        .chain(13..17)
        .chain(20..23)
        .chain(34..37)
        .chain(103..108);
    for i in ints {
        println!("{}", buzzer.get(i));
    }
}

struct Buzzer {
    lookup: HashMap<usize, String>,
}

impl Buzzer {
    
    pub fn new() -> Buzzer {
        let lookup = HashMap::from([
            (3, String::from("Fizz")),
            (5, String::from("Buzz")),
        ]);
        Buzzer{lookup}
    }

    pub fn with_bazz() -> Buzzer {
        let mut out = Buzzer::new();
        out.lookup.insert(7, String::from("Bazz"));
        out 
    }
    
    pub fn get(&self, i: usize) -> String {
        let mut part = String::from("");
        for (k, v) in self.lookup.iter() {
            if i % k == 0 {
                part.push_str(v);
            }
        }
        if part.len() == 0 {
            part.push_str(&format!("{}", i));
        }
        part
    }
}
